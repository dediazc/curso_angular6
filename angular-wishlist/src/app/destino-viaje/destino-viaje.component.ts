
import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from '@angular/core';
import { DestinoViaje } from './../model/destino-viaje.models';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input("idx") position: number;  /* Se le asigna un nombre personalizado  (idx)  */
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() Clicked: EventEmitter<DestinoViaje>;

  constructor() {
    this.Clicked = new EventEmitter();
  }

  ngOnInit() {
  }

  ir(){
  	this.Clicked.emit(this.destino);
  	return false;
  }
}