import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { DestinoViaje } from '../model/destino-viaje.models';

// Autocompletado
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { fromEvent } from 'rxjs';


@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})

export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;

  minLongitud = 3;

  searchResults: string[]; // Autocompletado

  constructor(private fb: FormBuilder) { 
    //inicializar
    this.onItemAdded = new EventEmitter();
    //vinculacion con tag html
    this.fg = this.fb.group({
      nombre: ['', 
                  Validators.compose([
                    Validators.required, 
                    this.nombreValidator,
                    this.nombreValidatorParametrizable(this.minLongitud)
                  ])],
      url: ['']
    });
    
    //observador de tipeo (sobre todo cambio que se realice en el formulario)
    this.fg.valueChanges.subscribe((form: any) =>{
      console.log('cambio el formulario: ', form);
    })
  }

  ngOnInit(): void {

    // Autocompletado
    //Definir archivo de prueba (Carpeta ==> assets ==> datos.json)
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
    .pipe(
       map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
       filter(text => text.length > 4),
       debounceTime(200),
       distinctUntilChanged(),
       switchMap(() => ajax('/assets/datos.json'))
     ).subscribe(ajaxResponse => {
           this.searchResults = ajaxResponse.response;
       });

  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  // Validaciones personalizadas
  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5){
      return {invalidNombre: true};
    }
    return null;
  }

  // Validador parametrizable
  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: Boolean } | null => {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < minLong){
     return {minLongNombre: true};
    }
    return null;
    }
  }

}

